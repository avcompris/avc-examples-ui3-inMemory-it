#!/bin/sh

# File: avc-examples-ui3-inMemory-it/.gitlab-ci/dump_docker_logs.sh

docker ps

docker ps | tail -n +2 | grep avc | awk '{print $1}' | tac | while read i; do

	docker logs "${i}";

done
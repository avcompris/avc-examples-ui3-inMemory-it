package net.avcompris.examples.ui3.web.it;

import static net.avcompris.commons3.it.utils.IntegrationTestUtils.getTestProperty;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

import java.io.File;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.NotImplementedException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SimplePageTest {

	private WebDriver driver;
	private String ui3URL;

	@BeforeEach
	public void setUpFirefox() throws Exception {

		final String seleniumServerURL = getTestProperty("selenium.server.url");
		final String seleniumDesiredCapabilities = getTestProperty("selenium.desiredCapabilities");

		System.out.println("selenium.server.url: " + seleniumServerURL);
		System.out.println("selenium.desiredCapabilities: " + seleniumDesiredCapabilities);

		final Capabilities capabilities;

		if ("firefox".contentEquals(seleniumDesiredCapabilities)) {
			capabilities = DesiredCapabilities.firefox();
		} else if ("chrome".contentEquals(seleniumDesiredCapabilities)) {
			capabilities = DesiredCapabilities.chrome();
		} else {
			throw new NotImplementedException("selenium.desiredCapabilities: " + seleniumDesiredCapabilities);
		}

		driver = new RemoteWebDriver(new URL(seleniumServerURL), capabilities);

		ui3URL = getTestProperty("ui3.url");

		System.out.println("ui3.url: " + ui3URL);
	}

	@AfterEach
	public void tearDownFirefox() throws Exception {

		if (driver != null) {

			try {

				driver.quit();

			} catch (final WebDriverException e) {

				// Sometimes, "driver.quit()" just crashes.
				//
				e.printStackTrace(); // log the error, and do nothing
			}
		}
	}

	private static void waitFor(final WebDriver driver, final String elementId) throws TimeoutException {

		final WebDriverWait wait = new WebDriverWait(driver, 10);

		wait.until(presenceOfElementLocated(By.id(elementId)));
	}

	@Test
	public void testLandingPage() throws Exception {

		final File destDir = new File("target/ui-it-results");

		FileUtils.forceMkdir(destDir);

		driver.get(ui3URL);

		Thread.sleep(2_000);

		waitFor(driver, "body-home");

		final File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(file, new File(destDir, "testLandingPage.png"));
	}
}

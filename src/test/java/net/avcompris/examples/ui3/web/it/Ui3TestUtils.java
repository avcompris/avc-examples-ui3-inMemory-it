package net.avcompris.examples.ui3.web.it;

import static java.util.Locale.ENGLISH;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.apache.commons.lang3.RandomUtils.nextInt;

public abstract class Ui3TestUtils {

	public static String randomUsername() {

		return (randomAlphabetic(1) + randomAlphanumeric(nextInt(7, 20))) //
				.toLowerCase(ENGLISH);
	}
}

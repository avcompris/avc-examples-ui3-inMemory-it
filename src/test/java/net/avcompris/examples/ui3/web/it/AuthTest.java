package net.avcompris.examples.ui3.web.it;

import static net.avcompris.commons3.databeans.DataBeans.instantiate;
import static net.avcompris.commons3.it.utils.IntegrationTestUtils.getTestProperty;
import static net.avcompris.examples.ui3.web.it.Ui3TestUtils.randomUsername;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import net.avcompris.commons3.api.User;
import net.avcompris.commons3.web.it.utils.Api;
import net.avcompris.commons3.web.it.utils.WebTestUtils;
import net.avcompris.examples.shared3.Role;
import net.avcompris.examples.users3.api.UserCreate;
import net.avcompris.examples.users3.client.UsersServiceClient;
import net.avcompris.examples.users3.core.api.UsersService;
import net.avcompris.guixer.core.AbstractUITest;
import net.avcompris.guixer.core.UIEnv;

@UIEnv(baseURL = "${ui3.url}")
public class AuthTest extends AbstractUITest {

	private User superadmin;
	private UsersService usersService;

	@BeforeEach
	public void setUp() throws Exception {

		final Api api = WebTestUtils.api(getTestProperty("api.baseURL"));

		usersService = api.forge(UsersService.class, "${users.baseURL}", UsersServiceClient.class);

		superadmin = api.user(getTestProperty("api.password"));
	}

	@Test
	public void testAuth_OK() throws Exception {

		final String username = randomUsername();
		final String password = randomAlphanumeric(8);

		usersService.createUser(null, superadmin, username, instantiate(UserCreate.class) //
				.setRole(Role.REGULAR) //
				.setPassword(password) //
				.setEnabled(true));

		assertNull(usersService.getUser(null, superadmin, username).getLastActiveAt());

		command("Sign in")

				.get() //
				.sleep(2_000) //
				.waitFor("#body-home") //

				.clear("#text-username") //
				.sendKeys("#text-username", username) //
				.clear("#password-password") //
				.sendKeysSecret("#password-password", password) //
				.takeScreenshot("form") //

				.click("#submit-login") //

				.sleep(6_000) //

				.waitFor("#body-home") //
				.waitFor("css:#div-body.authenticated") //

				.takeScreenshot("logged_in");

		assertNotNull(usersService.getUser(null, superadmin, username).getLastActiveAt());
	}

	@Test
	public void testAuth_FAIL() throws Exception {

		final String username = randomUsername();
		final String password = randomAlphanumeric(8);

		usersService.createUser(null, superadmin, username, instantiate(UserCreate.class) //
				.setRole(Role.REGULAR) //
				.setPassword(password) //
				.setEnabled(true));

		assertNull(usersService.getUser(null, superadmin, username).getLastActiveAt());

		command("Go to the landing page")

				.get() //
				.sleep(2_000) //
				.waitFor("#body-home") //

				.clear("#text-username") //
				.sendKeys("#text-username", username) //
				.clear("#password-password") //
				.sendKeysSecret("#password-password", "coco") //
				.takeScreenshot("form") //

				.click("#submit-login") //

				.sleep(6_000) //

				.waitFor("#body-home") //

				.takeScreenshot("auth_error");

		assertNull(usersService.getUser(null, superadmin, username).getLastActiveAt());
	}

	@Test
	public void testLogout() throws Exception {

		final String username = randomUsername();
		final String password = randomAlphanumeric(8);

		usersService.createUser(null, superadmin, username, instantiate(UserCreate.class) //
				.setRole(Role.REGULAR) //
				.setPassword(password) //
				.setEnabled(true));

		command("Sign in")

				.get() //
				.sleep(2_000) //
				.waitFor("#body-home") //

				.clear("#text-username") //
				.sendKeys("#text-username", username) //
				.clear("#password-password") //
				.sendKeysSecret("#password-password", password) //
				.takeScreenshot("form") //

				.click("#submit-login") //

				.sleep(6_000) //

				.waitFor("#body-home") //
				.waitFor("css:#div-body.authenticated") //

				.takeScreenshot("logged_in");

		command("Sign out")

				.click("#link_sign_out") //

				.sleep(2_000) //

				.waitFor("#body-home") //

				.takeScreenshot("logged_out");
	}
}
